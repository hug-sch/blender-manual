
################
  Filter Nodes
################

Filters process the pixels of an image to highlight additional details or perform some sort of
post-processing effect on the image.

.. toctree::
   :maxdepth: 1

   blur/index.rst

----------

.. toctree::
   :maxdepth: 1

   anti_aliasing.rst
   denoise.rst
   despeckle.rst

----------

.. toctree::
   :maxdepth: 1

   dilate_erode.rst
   inpaint.rst

----------

.. toctree::
   :maxdepth: 1

   filter.rst
   glare.rst
   pixelate.rst
   posterize.rst
   sun_beams.rst
